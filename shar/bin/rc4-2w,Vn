#!/usr/bin/env perl
# TBD : sub routine ordering better, still needs work to reflect flow order

$^W=1;
  # equivalent to 'perl -w'
use strict;

use Getopt::Std;
use Crypt::RC4;
use File::Find ();
use Carp;

# ====================================================================
# RCS INFORMATION (do not touch fields bounded by $)
# --------------------------------------------------------------------
#     $Date: 2012/02/04 14:19:13 $   (GMT)
# $Revision: 1.29 $
#   $Author: rodmant $ (aka last changed by)
#   $Locker: rodmant $
#   $Source: /usr/local/7Rq/package/cur/main-2010.11.26/lib/sh/RCS/_29r_sh.m4.sed-src,v $
#      $Log: _29r_sh.m4.sed-src,v $
#      Revision 1.29  2012/02/04 14:19:13  rodmant
#      *** empty log message ***
#
#      Revision 1.28  2011/05/22 11:40:52  rodmant
#      *** empty log message ***
#
#      Revision 1.27  2011/05/21 13:05:38  rodmant
#      *** empty log message ***
#
#      Revision 1.26  2011/05/21 12:18:04  rodmant
#      *** empty log message ***
#
#      Revision 1.17  2010/10/23 23:44:15  rodmant
#      *** empty log message ***
#
#      Revision 1.16  2010/10/23 12:46:34  rodmant
#      *** empty log message ***
#
# --------------------------------------------------------------------

# --------------------------------------------------------------------
# Synopsis: Encrypt/decrypt text using RC4. Supports a scheme
#   for host password encrypt/decryption.  For usage:
#   $ourname --help 
# --------------------------------------------------------------------
# Rating: tone: tool used: often stable: y TBDs: y noteable: y
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) 2010 Tom Rodman <Rodman.T.S@gmail.com>
# --------------------------------------------------------------------

# == Software License ==
# This file is part of uqjau.
#
# uqjau is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# uqjau is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with uqjau.  If not, see <http://www.gnu.org/licenses/>.
# ---

our($_29team);
  # value set in dirdefs require in BEGIN block

my ($_29r,$_29rev,$ourname,$USER,$passphrase,$config_dir,$default_config_dir);
my ($pw_index,$default_key_file,$default_domain_file,@match);
our ($opt_N, $opt_L,$opt_l,$opt_m,$opt_P,$opt_p,$opt_C,$opt_D,$opt_D,$opt_u,$opt_d,$opt_c,$opt_k,$opt_K);
  # needed by getopts
my(@domainpw_match,%domain);

use Cwd 'realpath';
BEGIN{
# --------------------------------------------------------------------
# define directory definitions for our world, in several env vars
# --------------------------------------------------------------------
  my ($startdir, $dirname);
  $startdir= realpath;

  $0 =~ m{^(.*)/} and $dirname=$1;
  chdir "$dirname/../.." || die;
    #cd to parent dir of parent dir of parent dir of $0
    #we die if $0 has no "/"; that's OK

  $_29r= realpath;

  chdir "$startdir" || die;

  ($_29rev = $dirname ) =~ s~^.*/~~;
    # basename of $dirname

  require "$_29r/package/$_29rev/main/etc/dirdefs.perl";

}

sub help 
{
print <<_end_of_here_doc_;
${ourname}:

Synopsis: Encrypt/decrypt text using RC4. Supports a scheme
  for host password encrypt/decryption.

  supporting files:
    KEYFILE
      A single line file containing a passphrase.  Required for any
      use of $ourname.

    Required for password lookups:
      PASSWORDFILE
        Each line has 5 colon delimited fields:

          USERNAME:SHORT_HOSTNAME:OPTIONAL_DOMAIN:OPTIONAL DESCRIPTION:RC4_ENCRYPTED_PASSWORD

      DOMAINFILE
        Each line w/two colon delimited fields.
          SHORT_HOSTNAME:DOMAINNAME

  OPTIONs applying to all modes:

     [-P] => personal CONFIG_DIR below \$HOME
       changes CONFIG_DIR to ~/_.pwm

       default CONFIG_DIR is $default_config_dir 
    
     [-p PASSWORDFILE ] 
       default is CONFIG_DIR/.p
       -p - => use STDIN 

     [-l DOMAINFILE] 
       default is CONFIG_DIR/login_domain

     [{-k KEYFILE}|{-K KEYSTRING}]:
       default KEYFILE is CONFIG_DIR/.k

   password lookups:

     -u {username:host}

     Example:

       $ourname -u smithj:host015
       # print password for user smithj on host015
   
     [-L] => want local account, ignore DOMAINFILE

   filter mode (*no* fields assumed):

     -C => encode STDIN 
       Example: echo here is any text|${ourname} -C |tee /tmp/encoded 

     -D => decode STDIN (*no* fields assumed)
       Example: ${ourname} -D < /tmp/encoded 

   password-filter mode (only password field processed):

     -mc and -md switches result in PASSWORDFILE
     being used as STDIN. Add "-p -" to force true filter
     behavior.

     -mc => encode  
       # think -m as in "maintenance"

       Example:
         echo root:host022::description:encrypted_pw::|$ourname -mc -p -

     -md => decode

   Example: adding new entry to passwd file:

     echo 'jsmith:foo.org:::{plain text passwd}:'|$ourname -mcPp -|
     tee /tmp/tmp_save|$ourname -mdPp -
       # -P   => personal config dir for (passphrase) keyfile, and login_domain file
       # -p - => password file is on STDIN
       # -mc  => encode a plain text password file
       # -dc  => decode a plain text password file
      
     # test a password lookup:
       $ourname -p /tmp/tmp_save -Pu jsmith:foo.org

_end_of_here_doc_
}

sub read_domain_file {
  my $domain_file  = shift (@_);
  my (@tmp,$tmp);

  open(DOMAIN,"< $domain_file") or 
    die ("$ourname:ERROR: can't open ${domain_file}: $!\n");

  local $/;
    # undefine "line record separator"

  $tmp= <DOMAIN>;
  $tmp =~ s/#.*\n/\n/mgs;
    # cleanup comments

  %domain = split(/\n|:/,$tmp);

  close(DOMAIN);
}

sub read_keyfile {
  # used by load_keyfile
  my $key_file  = shift (@_);
  my $FUNCNAME = (caller(0))[3];
     # name of this subroutine

  open(KEY,"< $key_file") or 
    die ("$ourname:$FUNCNAME:ERROR: can't open ${key_file}: $!\n");

  local $/;
    # undefine "line record separator"
  $passphrase = <KEY>;

  $passphrase =~ s/#.*\n/\n/mgs;
    # cleanup comments

  close(KEY);
}

sub load_keyfile {
  my $FUNCNAME = (caller(0))[3];
    # name of this subroutine

  if ($opt_k) 
  { read_keyfile $opt_k }
  elsif (-s $default_key_file ) 
  { read_keyfile $default_key_file }
  else 
  { print STDERR "$FUNCNAME:ERROR: no -k {keyfile}\n";
    exit 1;
  }
}

sub get_host_or_domain {
  my $FUNCNAME = (caller(0))[3];
    # name of this subroutine
  my ($host, $login_domain) = @_;

  if ($host and $login_domain) 
  {
    print STDERR 
      "ERROR:$FUNCNAME: only 1 of host [$host] or login domain [$login_domain] may be defined\n\n";
    exit 1;
  }
  elsif ($host or $login_domain) 
  { return ($host or $login_domain); }
  else 
  {
    print STDERR "ERROR:$FUNCNAME: neither host nor login_domain defined\n\n";
    exit 1;
  }
}

sub unprotect_chars 
{
  # required by decode_entire_line
  my $scale_ref    = shift (@_) ;

  $$scale_ref  =~ s/_colon_/:/g; 
  $$scale_ref  =~ s/_carriage_/\r/g; 
  $$scale_ref  =~ s/_newline_/\n/g; 
}

sub decode_pw_field {
  # first arg == a reference to an array with lines in our "rc4 passwordfile" format
  # 2nd arg   == optional, if it has any value then $want_password_only flag is set

  # Acts on lines in @$in_array_ref, which is assumed to be in our "rc4
  # passwordfile" format.  This function decodes the password field on
  # each line, and then either prints the entired line to STDOUT or if
  # $want_password_only is true, only the password is printed.

  my $FUNCNAME = (caller(0))[3];
    # name of this subroutine
  my ($in_array_ref,$want_password_only) = @_ ;

  my ($coded_pw,$host_or_domain,@fields,$decoded_pw);

  for (@$in_array_ref) {
    print,next if /^\s*#/;
    print,next if /^\s*$/;
    last if (/^__END__/);
    chomp;
    @fields = split(/:/,$_) ;

    my ($host,$login_domain) = @fields[1,2] ; 

    $host_or_domain = get_host_or_domain ($host,$login_domain) ;

    $coded_pw =  $fields[$pw_index] ;

    if (! $coded_pw ) { 
      print "$ourname:$FUNCNAME: ERROR: empty password field in [@$in_array_ref]\n";
      exit 1;
    }

    ## restore troublesome chars
      unprotect_chars \$coded_pw ;

      $decoded_pw = 
        $fields[$pw_index] = RC4( "$passphrase$host_or_domain", $coded_pw) ;

    if($want_password_only) {

      if ($opt_N)
      { print "$decoded_pw";}
      else
      { print "$decoded_pw\n";}

    } else
    {
      print join(":",@fields) . "\n";
    }
  }
}

sub password_lookup_and_print {
  # lookup $user and $host, in password lines, decode password and print to STDOUT
  # Should be only one match, one answer, one password printed.

  ## command line has -u USERNAME:HOSTNAME
  my ($user,$host) = (split(":",$opt_u))[0,1] ; 

  my ($cnt,$regxp,$login_domain,@pw_file,$Domain);

  # read all "RC4 password" lines
  while (<>) {
    next if /^\s*($|#)/;
    close ARGV if (/^__END__/);
    push(@pw_file,$_);
  }

  if (! $host ) {
    print STDERR "got [$opt_u], need hostname in 2nd (colon delimited) field\n"; 
    exit 1;
  }

  if ($opt_L) {
    # => ignore host's login_domain field.
    # Password will probably only apply to this host.
    $regxp = "${user}:$host:" ;
    @match =  grep (m{$regxp}, @pw_file);
  } 
  elsif ($Domain = $domain{$host}) {
    # $host in login_domain
    $regxp = "${user}::$Domain";
    @match =  grep (m{$regxp}, @pw_file);
  } 
  elsif (@match =  grep (m<${user}:${host}:>, @pw_file)){
    # $host not in a larger login_domain
    1;
  }
  else {
    print STDERR "$ourname:ERROR: [$host] apparently not in a login_domain.\n";
    print STDERR "  Also could not find non-domain host, search for: ${user}:${host}.\n";
    exit 1;
  }
    
  $cnt = @match;
  
  if ($cnt > 1 ) {
    print STDERR "$ourname:ERROR: too many matches for [$regxp]\n";
    print STDERR "  [@match]\n";
    exit 1;
  }
  elsif ($cnt == 0 ) {
    print STDERR "$ourname:ERROR: no match for [$regxp]\n";
    exit 2;
  }

  decode_pw_field (\@match,"just_password");
    # prints decoded password to STDOUT

  ## end of password_lookup_and_print
} 

sub protect_chars 
{
  # required by encode_entire_line
  my $scale_ref    = shift (@_) ;

  $$scale_ref  =~ s/:/_colon_/g; 
    # : used field sep in PASSWORDFILE
  $$scale_ref  =~ s/\n/_newline_/gms; 
  $$scale_ref  =~ s/\r/_carriage_/gms; 
}

sub encode_entire_line  {
  # Every character in the string we are passed is encrypted, then printed to STDOUT.

  my $in = shift(@_);
  chomp $in;

  my $encoded_stg = RC4($passphrase,$in);

  protect_chars \$encoded_stg ;
    # protect troublesome chars

  print "${encoded_stg}\n";
}

sub decode_entire_line  {
  # Every character in the string we are passed is decrypted, then printed to STDOUT.
  my $in = shift(@_);
  my $out;

  chomp $in;
  unprotect_chars \$in;
    # unprotect troublesome chars

  print RC4($passphrase,$in) . "\n";
}

sub encode_pw_field_filter  {
  # Acts on <>. When on "password lines" of our "rc4 password
  # line" (lines w/a plain text, ie non encrypted password), this
  # function encodes password field.  Comment lines or blanks are
  # unchanged.

  my ($host,$coded_pw,$login_domain,$host_or_domain,@fields);
  $| = 1; 
    # needed on linux (output auto flush)

  while(<>) {
    print,next if /^\s*($|#)/;
      # show but don't encode comment or blank lines
    close ARGV,next if (/^__END__/);

    chomp;
    @fields = split(/:/);

    ($host,$login_domain) = @fields[1,2];
    $host_or_domain = get_host_or_domain ($host,$login_domain) ;
      # host_or_domain to lessen duplicate encrypted passwords

    $coded_pw = RC4("$passphrase$host_or_domain", $fields[$pw_index]);

    # protect troublesome chars
    protect_chars \$coded_pw;

    $fields[$pw_index] = $coded_pw ;

    print join(":",@fields) . "\n";
  }
}

# ==================================================================== 
# config file lookup related functions
# ==================================================================== 

# for the convenience of &wanted calls, including -eval statements:
use vars qw/*name *dir *prune/;
*name   = *File::Find::name;
*dir    = *File::Find::dir;
*prune  = *File::Find::prune;

my @matches;

sub wanted {
  my ($dev,$ino,$mode,$nlink,$uid,$gid);
  my $ignoreRegex = q{\@(hold|burn)};

  /^${ignoreRegex}\z/s &&
  ($File::Find::prune = 1)
  ||
  ($nlink || (($dev,$ino,$mode,$nlink,$uid,$gid) = lstat($_))) &&
  -d _ &&
  /^${main::confFileBaseNameRegex}\z/s &&
  push (@matches, $name);
}

sub getConfigDirBelow
{
  # Searches for ${main::confFileBaseNameRegex} below $configSearchDir, returns config dir pathname.

  my $configSearchDir = shift;
  my $subname = (caller(0))[3];

  if (! defined $configSearchDir) {
    carp  "$subname: ERROR: expected 1st argument to be a directory, instead got [undef].\n";
  }
  elsif (! -d $configSearchDir) {
    carp  "$subname: ERROR: [$configSearchDir] is not a directory.\n";
  }

  # Core step:
  File::Find::find({wanted => \&wanted}, $configSearchDir);

  if ( scalar @matches == 1 ) {
    return $matches[0];
  }
  else {
    print  STDERR "$subname: ERROR: config file could not be identified.\n";
    printf STDERR "          file matches found: %d (expecting 1).\n", scalar @matches;
    return;
  }
}

# ====================================================================
## main ##
# ====================================================================

($ourname = $0 ) =~ s:^.*/::g; 
  # get my own name
my $pw_file_basen = ".rc4-2w";
${main::confFileBaseNameRegex} = quotemeta( $pw_file_basen );

$pw_index = 4; 
  # passwd is this field (starting w/field zero)

$default_config_dir= "$_29team/$pw_file_basen"; 
  # centralized/shared w/staff

if (($ARGV[0] =~ /\-\-help/i ) or (! @ARGV ) ) 
{ &help; exit 0 }

getopts('NLlmPp:CDu:dck:K:');


if($opt_P) {
  our $HOME=$ENV{"HOME"};
  $config_dir = "$HOME/$pw_file_basen";
}
else { 
  $config_dir= getConfigDirBelow "$_29team";
}

$default_key_file =    "$config_dir/.k" ;
$default_domain_file = "$config_dir/login_domain" ;

if($opt_p)     
{ @ARGV = ("$opt_p") }
else
{ #------------------------------------------------------------------
  # allow cat-ing of password files like these three for example:
  #  .p .p.wk .p.hm 
  #------------------------------------------------------------------
    opendir(DIRHANDLE,"$config_dir") or die 
      "$ourname:ERROR: couldn't open dir: $config_dir : $!\n";
    @ARGV = grep (/^\.p(\.\w+)?$/ && ! /\.(swp|bak)$/i, readdir(DIRHANDLE));
      # (.swp is the vim swap file extension, so we skip those)
    map(s:^:$config_dir/:,@ARGV); 
      # prepend $config_dir
    closedir(DIRHANDLE);
}

# read domain file
if ($opt_l) 
{ read_domain_file $opt_l }
elsif (-s $default_domain_file ) 
{ read_domain_file $default_domain_file  }

# define master RC4 $passphrase
if ($opt_K) { $passphrase = $opt_K }
else        { load_keyfile }

# --
# initialization done

# #################################################################### 
# actions 
#   for -u or -C or -D
# #################################################################### 

if($opt_u)
{ password_lookup_and_print;
    # lookup $user and $host, decode password and print to STDOUT
  exit 0;
}
elsif ($opt_C) 
{ @ARGV = ("-");
  encode_entire_line $_ while(<>);
    # *every* character in $_ is encrypted.
    # Results to STDOUT.

  exit 0;
}
elsif ($opt_D) 
{ @ARGV = ("-");
  decode_entire_line $_ while(<>);
    # *every* character in $_ is decrypted.
    # Results to STDOUT.
  
  exit  0;
}

# ********************************************************************
# -maintenance mode-:
#   -m
#   act as a code (-mc) or decode (-md) RC4-password-file-filter:
# ********************************************************************

if($opt_m) 
{ if ($opt_c) 
  { encode_pw_field_filter;
      # Acts on <>. When on "password lines": encodes password field.
      # Comment lines or blanks are unchanged.
      # Results to STDOUT.
    exit;
  }
  elsif ($opt_d) 
  { my @text_in;  
      # interesting that 'my' works, even though 'decode_pw_field' needs @text_in.
    while (<>)
    { close ARGV, next if (/^__END__/);
      push(@text_in,$_);
    }

    decode_pw_field \@text_in ;
      # Decodes pw field leaving other fields alone.
      # Results to STDOUT.
    exit;
  }
  else { help; exit 2 }
}

##
help; exit 2; 

########################## end of script ###########################
__END__

